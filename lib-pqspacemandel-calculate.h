// ======================================================================
// PQ space: Fractal of Mandelbrot type
// (CC) 2011 Jens Koeplinger, http://www.jenskoeplinger.com/P
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "pqspacemandelbrotfractal.c" for what this means to you!
// ======================================================================
// lib-pqspacemandel-calculate.h
// ----------------------------------------------------------------------
// Calculate a fractal in PQ space that is of Mandelbrot type:
//     t_(n+1) = ( t_n + c )^2
// where c is a constant corresponding to the pixel position of the
// output bitmap.
//
// Example:
/*

    calculatePQspaceMandelbrotTypeFractal();
       (...)

*/
// ---------------------------------------------------------------------- 

void calculatePQspaceMandelbrotTypeFractal() {
//  This function takes a seed value, adds a constant to it based on the
//  position of the pixel in the output bitmap, and then squares the value
//  according to multiplication in PQ space. This procedure is repeated
//  iteratively, until:
//    (1) a threshold iteration depth is met, or
//    (2) a threshold radius is exceeded (indicating a divergent series).
//  When condition (1) is met, the point is assumed to correspond to a
//  convergent iterative series, if condition (2) is met, the point must
//  correspond to an iterative series. Colorization is set accordingly,
//  and an output bitmap in XPM 2 format is created.

    FILE    *fp;                      // file pointer for wspace.xpm bitmap file

    int     this_X;                   // bitmap X position (pixel)
    int     this_Y;                   // ... Y position
    int     this_Depth;               // current iteration depth
    double  pN;                       // iteration variable, p value
    double  qN;                       // ... q value
    double  thisAngle;                // Euclidean angle of current point
    double  thisU;                    // PQ space angle (from polar form)
    double  thisModulus;              // modulus of (p, q) point
    double  thisRadius;               // radius of current point
    double  pPix;                     // p value of bitmap pixel position
    double  qPix;                     // ... q value

    double  thisModulusDenom;         // throwaway
    double  thisModulusEnum;          // throwaway

    printf("Left bitmap boundary (low p): %f\n", p_left);
    printf("Right boundary (high p):      %f\n", p_right);
    printf("Bottom boundary (low q):      %f\n", q_bottom);
    printf("Top boundary (high q):        %f\n", q_top);
    printf("Output bitmap size (pixels):  %d x %d\n", outFileWidth, outFileHeight);
    printf("Iteration cutoff (depth):     %d\n", thresholdDepth);
    printf("Seed value:             p_0 = %f\n", p_seed);
    printf("                        q_0 = %f\n", q_seed);
    printf("Radius function:          r = ");

    if (radiusFunction == 0) {
        printf("abs(cos(2u))\n");
    } else if (radiusFunction == 1) {
        printf("cos(2u)\n");
    } else if (radiusFunction == 2) {
        printf("cos(u)\n");
    } else {
        printf("other (%d)\n", radiusFunction);
    }
    
    printf("Additional angle offset:      %f\n", angleShift);
    printf("Divergence cutoff:            %f\n", divergenceCut);

    fp = fopen("pqspaceMandel.xpm", "wb");
    if (fp == NULL) {
        printf("PANIC - file 'pqspaceMandel.xpm' didn't open\n\n**-BP4-**\n");
        return;
    }

    // print file header (type, size, pallette)
    fprintf(fp, "! XPM2\n");
    fprintf(fp, "%d %d", outFileWidth, outFileHeight);
    writeColorTemplateToFile(fp);

    // reset bitmap Y axis counter (rows) to Zero and start calculation
    this_Y = 0;

    while (this_Y < outFileHeight) {

        this_X = 0;

        while (this_X < outFileWidth) {
            // compile the iteration start values

            this_Depth           = 0 ; // current iteration depth: start
            thisModulus          = 0.;
            thisRadius           = 0.;
            pN                   = p_seed; // set iteration value to seed values
            qN                   = q_seed;
            pPix                 = p_left + ((p_right - p_left) * ((double) this_X) / ((double) outFileWidth ));
            qPix                 = q_top  - ((q_top - q_bottom) * ((double) this_Y) / ((double) outFileHeight));

            // perform the first step in the iteration manually: add the current
            // pixel's (p, q) value to the iteration parameter;
            pN = pN + pPix;
            qN = qN + qPix;

            // now, begin the iteration, up to the cutoff conditions:
            //   (1) abs(radius) too high, or
            //   (2) iteration threshold exceeded

            while ((fabs(thisRadius) < divergenceCut) && (this_Depth < thresholdDepth)) {
                // iteration: determine radius and angle, square, add pixel position

                // determine Euclidean angle from arctangens, then convert into
                // PQ space angle thisU depending on chosen angle function
                thisAngle = atan2(qN, pN);

                if (radiusFunction == 0) {
                    // r = |cos(2u)|

                    thisU = thisAngle + (piD / 4.) + angleShift;
                    thisU = thisU * 2.;               // squaring
                    thisAngle = thisU - ((piD / 4.) + angleShift);

                } else if (radiusFunction == 1) {
                    // r = cos(2u)

                    if (thisAngle < 0.) thisAngle = thisAngle + (2. * piD);

                    if ((thisAngle > (piD / 4.)) && (thisAngle < (3. * piD / 4.))) {
                        // top q lobe, add pi

                        thisAngle = thisAngle + piD;

                    } else if ((thisAngle > (5. * piD / 4.)) && (thisAngle < (7. * piD / 4.))) {
                        // bottom q lobe, subtract pi

                        thisAngle = thisAngle - piD;

                    }

                    thisU = thisAngle + (piD / 4.) + angleShift;
                    thisU = thisU * 2.;               // squaring
                    thisAngle = thisU - ((piD / 4.) + angleShift);

                } else if (radiusFunction == 2) {
                   // r = cos(u), the complex number case which is the standard Mandelbrot set

                   thisAngle = thisAngle * 2.;       // square

                }

                // determine square of the modulus from the quadrifolium:
                // |Z|^2 := (pN^2 + qN^2)^3 / (pN^2 - qN^2)^2
                thisModulusEnum  = (pN * pN) + (qN * qN);
                thisModulusDenom = (pN * pN) - (qN * qN);
                thisModulus      = (thisModulusEnum * thisModulusEnum * thisModulusEnum)
                                   / (thisModulusDenom * thisModulusDenom);

                // calculate the new point: radius r = |cos(2u)| or r = cos(2u)
                // (note: for the second choice, radiusFunction == 1, it might be negative)
                // or standard Mandelbrot set over the complexes r = cos(u)

                if (radiusFunction == 0) {
                    // modulus in PQ space with absolute function
  
                    thisRadius = thisModulus * fabs(cos(2. * thisAngle));

                } else if (radiusFunction == 1) {
                    // modulus in PQ space allowing negative cos(2u) values
  
                    thisRadius = thisModulus * cos(2. * thisAngle);

                } else if (radiusFunction == 2) {
                    // modulus in the complex numbers is just the unit circle
    
                    thisRadius = (pN * pN) + (qN * qN);
    
                }

                pN         = thisRadius  * cos(thisAngle);
                qN         = thisRadius  * sin(thisAngle);

                // add the bitmap position (for Mandelbrot type fractal)
                pN = pN + pPix;
                qN = qN + qPix;

                // point squared, bitmap position added, radius determined:
                // move on (if conditions OK)
                this_Depth++;

            }

            if (this_Depth == thresholdDepth) {
                // convergent series (at least until threshold)

                fprintf(fp, "A");

            } else {
                // divergent series; plot shade of gray

                fprintf(fp, "%d", (this_Depth % 10));

            }

            // pixel finished; next one
            this_X++;

        }

        // print new line to file and status update to screen
        fprintf(fp, "\n");
        this_Y++;

        if ((this_Y % 10) == 0) {
            // print status note every 10 rows

            printf("... finished row %d of %d\n", this_Y, outFileHeight);

        }

    }

    // done - output written
    // close the file

    fclose(fp);

    // iteration complete

    printf("Bitmap complete.\n");

}

