// ===========================================================================
// PQ space: Fractal of Mandelbrot type
// (CC) 2011 Jens Koeplinger
// http://www.jenskoeplinger.com/P
//
// Reference: J. A. Shuster, J. Koeplinger,
// "Doubly nilpotent numbers in the 2D plane" (2010), Appendix A.
// http://jenskoeplinger.com/P/PaperShusterKoepl-PQSpace.pdf
//
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
//
// You are free:
// - to Share - to copy, distribute and transmit the work
// - to Remix - to adapt the work
//
// Under the following conditions:
// - Attribution. You must attribute the work in the manner specified by the
//   author or licensor (but not in any way that suggests that they endorse
//   you or your use of the work).
// - Share Alike. If you alter, transform, or build upon this work, you may
//   distribute the resulting work only under the same, similar or a compatible
//   license.
//
// For any reuse or distribution, you must make clear to others the license
// terms of this work. The best way to do this is with a link to:
//   http://creativecommons.org/licenses/by-sa/3.0
//
// Any of the above conditions can be waived if you get permission from the
// copyright holder.
//
// Nothing in this license impairs or restricts the author's moral rights.
// ===========================================================================

#define PQspaceToolVersion "PQ space - Fractal of Mandelbrot type, version 0.1.0 (16 January 2011)\n"
#define licenseMain        "(CC) 2011 Jens Koeplinger - http://www.jenskoeplinger.com/P\n"
#define licenseDetail      "License: Creative Commons Attribution - Share Alike 3.0 Unported\n"
#define licenseReference   "http://creativecommons.org/licenses/by-sa/3.0\n\n"

#define piD                3.141592653589793

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

// ---------------------------------------------------------------------------
// global declarations - functions
// ---------------------------------------------------------------------------

//lib-pqspacemandel-calculate.h
void   calculatePQspaceMandelbrotTypeFractal();

// lib-pqspacemandel-XPM.h
void   writeColorTemplateToFile(FILE *);

// ---------------------------------------------------------------------------
// global declarations - variables
// ---------------------------------------------------------------------------

double p_left         =   -2. ;   // lower (left) parameter bound (p axis)
double p_right        =    2. ;   // ... high bound
double q_bottom       =   -2. ;   // lower (bottom) parameter bound (q axis)
double q_top          =    2. ;   // ... upper bound
int    outFileWidth   =  800  ;   // pixels width of output bitmap
int    outFileHeight  =  800  ;   // ... height
int    thresholdDepth = 2000  ;   // iteration cut-off
double p_seed         =    0. ;   // iteration seed p parameter
double q_seed         =    0. ;   // ... q seed
int    radiusFunction =    0  ;   // type of radius function; 0: abs(cos(2u))
double angleShift     =    0. ;   // additional shift in angle between t and u
double divergenceCut  =   10. ;   // divergence cutoff modulus value

// ---------------------------------------------------------------------------
// main()
// ---------------------------------------------------------------------------

int main(int argc, char **argv) {

    int i; // throwaway

    printf(PQspaceToolVersion);
    printf(licenseMain);
    printf(licenseDetail);
    printf(licenseReference);
    printf("For HELP, run without command-line parameters.\n");
    printf("\nFor documentation on the PQ space fractal of Mandelbrot type, see:\n");
    printf("   J. A. Shuster, J. Koeplinger,\n");
    printf("   \"Doubly nilpotent numbers in the 2D plane\" (2010), Appendix A.\n");
    printf("   http://jenskoeplinger.com/P/PaperShusterKoepl-PQSpace.pdf\n\n");

    // read command line parameters
    if ((argc == 1) || (argv[1][0] == '-')) {
        //  no command line argument given,
        // or first argument started with dash;
        // --> print help and abort

#include "help-pqspacemandelbrotfractal.c"

        return;

    }

    //  at least one command line argument given; display raw
    printf("-------------------------------------\n");
    printf("Number of command line arguments = %d\n", argc);

    for (i = 0; i < argc; i++) {
        printf("- Arg. #%d = \"%s\"\n", i, argv[i]);
    }

//  (naively) parse command line parameters, if present, and
//  convert them, and overwrite global variables

    if (argc >  1) outFileWidth    = atoi(argv[ 1]);
    if (argc >  2) p_left          = atof(argv[ 2]);
    if (argc >  3) p_right         = atof(argv[ 3]);
    if (argc >  4) q_bottom        = atof(argv[ 4]);
    if (argc >  5) q_top           = atof(argv[ 5]);
    if (argc >  6) thresholdDepth  = atoi(argv[ 6]);
    if (argc >  7) p_seed          = atof(argv[ 7]);
    if (argc >  8) q_seed          = atof(argv[ 8]);
    if (argc >  9) radiusFunction  = atoi(argv[ 9]);
    if (argc > 10) {
         angleShift = atof(argv[10]);
         angleShift = angleShift * piD / 180.;
    }
    if (argc > 11) divergenceCut   = atof(argv[11]);

//  calculate output bitmap height dynamically:
    outFileHeight = (int) (((double) outFileWidth) * (q_top - q_bottom) / (p_right - p_left));

    if (argc > 12) {
    //  ignore any additional arguments, but print a warning

        printf("[INFO] More than 10 arguments provided; ignoring the additional ones.\n");

    }

    calculatePQspaceMandelbrotTypeFractal();

    printf("Good-bye\n");

}

// ---------------------------------------------------------------------------

#include "lib-pqspacemandel-calculate.h"
#include "lib-pqspacemandel-XPM.h"

// ---------------------------------------------------------------------------
