// ======================================================================
// PQ space: Fractal of Mandelbrot type
// (CC) 2011 Jens Koeplinger, http://www.jenskoeplinger.com/P
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "pqspacemandelbrotfractal.c" for what this means to you!
// ======================================================================
// help-pqspacemandelbrotfractal.c
// ----------------------------------------------------------------------
// prints help to stdout

    printf("This program calculates a fractal of Mandelbrot-type, in PQ\n");
    printf("space. Beginning with a (fixed) seed value, a value corresponding\n");
    printf("to the (x, y) coordinate on the screen is added, and the result\n");
    printf("is squared. This is repeated (add, square, add, square, and so on)\n");
    printf("until one of two cut-off conditions is met: (1) a threshold radius\n");
    printf("is exceeded that guarantees that the result of the iteration must\n");
    printf("be divergent, or (2) a maximum iteration steps ('depth') condition\n");
    printf("is met.\n\n");
    
    printf("The standard Mandelbrot set over the complex numbers can also\n");
    printf("be calculated, for comparison.\n\n");

    printf("Colorization of the output bitmap is then achieved as follows: If\n");
    printf("condition (1) is me (divergent series), a shade of gray is plotted\n");
    printf("to indicate how 'deep' the iteration went before aborting. If condition\n");
    printf("(2) is met, a dark blue point is set.\n\n");

    printf("Output format of the bitmap is XPM 2.\n\n");

    printf("Running this program without any parameters prints this help. Command\n");
    printf("line parameters are:\n");
    printf("  [1]  width in pixels         [        800 ],\n");
    printf("  [2]  left coordinate (low p) [default: -2.],\n");
    printf("  [3]  right (high p)          [          2.],\n");
    printf("  [4]  bottom (low q)          [         -2.],\n");
    printf("  [5]  top (high q)            [          2.],\n");
    printf("  [6]  recursion depth (cutoff)[       2000 ],\n");
    printf("  [7]  seed p                  [          0.],\n");
    printf("  [8]  seed q                  [          0.],\n");
    printf("  [9]  radius function:                       \n");
    printf("         0: r = |cos(2u)|      [     default],\n");
    printf("         1: r =  cos(2u),                     \n");
    printf("         2: r =  cos(u) (standard Mandelbrot),\n");
    printf("  [10] angle offset (degrees)  [          0 ],\n");
    printf("  [11] divergence cutoff       [         10.].\n\n");

    printf("Example: The following command calculates the fractal in a\n");
    printf("1000x1000 bitmap, p axis from -1.5 to +1.5, q axis from -1.5 to +1.5,\n");
    printf("with a recursion cutoff of 600:\n\n");

    printf("    ./calcPapillon 1000 -1.5 1.5 -1.5 1.5 600\n\n");
