// ======================================================================
// PQ space: Fractal of Mandelbrot type
// (CC) 2011 Jens Koeplinger, http://www.jenskoeplinger.com/P
// License: Creative Commons Attribution "Share Alike 3.0 Unported"
// http://creativecommons.org/licenses/by-sa/3.0
// See main file "pqspacemandelbrotfractal.c" for what this means to you!
// ======================================================================
// lib-pqspacemandel-XPM
// ----------------------------------------------------------------------
// some support functions relating to graphics and XPM bitmap output.
//
// Example:
/*

    writeColorTemplateToFile(fp);

*/

// ----------------------------------------------------------------------
// write the color palette to file
// ----------------------------------------------------------------------

void writeColorTemplateToFile(FILE *fp) {
// IN:  *fp is a pointer to the current file that's open for writing
//
// This function writes the color palette to the XPM file
//
// Description of color palette
// -----------------------------
//
// 0:   white #D0D0D0
// 1...8: shades of gray between #D0D0D0 and #404040
// 9:   gray  #404040
// A:   dark blue #000040

    fprintf(fp, " 11 1\n");
    fprintf(fp, "0 c #D0D0D0\n");
    fprintf(fp, "1 c #C0C0C0\n");
    fprintf(fp, "2 c #B0B0B0\n");
    fprintf(fp, "3 c #A0A0A0\n");
    fprintf(fp, "4 c #909090\n");
    fprintf(fp, "5 c #808080\n");
    fprintf(fp, "6 c #707070\n");
    fprintf(fp, "7 c #606060\n");
    fprintf(fp, "8 c #505050\n");
    fprintf(fp, "9 c #404040\n");
    fprintf(fp, "A c #000040\n");

}
